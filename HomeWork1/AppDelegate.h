//
//  AppDelegate.h
//  HomeWork1
//
//  Created by Дмитрий Дубровин on 16.08.16.
//  Copyright © 2016 Dubrovin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

