//
//  main.m
//  HomeWork1
//
//  Created by Дмитрий Дубровин on 16.08.16.
//  Copyright © 2016 Dubrovin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
